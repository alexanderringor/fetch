import React, {Component} from 'react'
import { BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom"
import Home from './Home'
import About from './About'
import Container from 'react-bootstrap/Container'



export default class RouterProject extends Component {

    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = { 
            result: []
        };
        
      }
    
    componentDidMount() {
           fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(data => this.setState({
                results: data
            }))
        }

    render() {
        return (
        
            <div>

            {
                // console.log(this.state.results)
            }
                
            
            <Router>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="./Home">Navbar</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
                <li className="mr-4">
                    <Link to="/home">Home</Link>
                </li>
                <li>
                    <Link to="/about">About</Link>
                </li>
            </ul>
            </div>
            </nav>
    
            <hr></hr>
    
    
            <Switch>
                <Route exact path="/home">
                    <Home/>
                </Route>
                <Route exact path="/about">
                    <About results={this.state.results}/>
                </Route>
            </Switch>
    
    
            
            </Router>

            {/* <Container fluid="true">
                <h1> Test</h1>
            </Container> */}
           
            </div>
            )
    }

    
    }
    