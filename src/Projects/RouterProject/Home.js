import React from 'react'
import Container from 'react-bootstrap/Container'
import Alert from 'react-bootstrap/Alert'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import { BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom"

export default function Home() {
    return (
        <Container fluid={true}>
        <Row>
            <Col lg="4">
                <h1>Home Page Left Pane</h1>
                <Link to="/about">About</Link>
                <Alert variant="dark">This is an Alert! </Alert>
            </Col>
            <Col lg="4"></Col>
            <Col lg="4">
                <h1>Home Page Right Pane</h1>
            </Col>
        </Row>
        </Container>
        )
    }
    