import React, { Component } from 'react'
// import Greeting from './Components/Greeting'
// import Welcome from './Components/Welcome'
// import Hello from './Components/Hello'
// import RouterProject from './Projects/RouterProject/RouterProject'
// import Message from './Components/Message'
// import Counter from './Components/Counter'
// import FunctionClick from './Components/FunctionClick'
// import ClassClick from './Components/ClassClick'
// import EventBind from './Components/EventBind'
// import ParentComponent from './Components/ParentComponent'
// import UserGreeting from './Components/UserGreeting'
// import NameList from './Components/NameList'
// import Stylesheet from './Components/Stylesheet'
// import Inline from './Components/Inline'
// import Form from './Components/Form'
// import LifecycleA from './Components/LifecycleA'
// import FragmentDemo from './Components/FragmentDemo'
// import Table from './Components/Table'
// import ParentComp from './Components/ParentComp'
// import RefsDemo from './Components/RefsDemo'
// import FocusInput from './Components/FocusInput'
// import PortalDemo from './Components/PortalDemo'
// import Hero from './Components/Hero'
// import ErrorBoundary from './Components/ErrorBoundary'
// import ClickCounter from './Components/ClickCounter'
// import HoverCounter from './Components/HoverCounter'
// import ClickCountertwo from './Components/ClickCounterTwo'
// import HoverCounterTwo from './Components/HoverCounterTwo'
// import User from './Components/User'
// import Counter from './Components/Counter'
// import { UserProvider } from './Components/userContext'
// import ComponentC from './Components/ComponentC'




export default class AppCodeVolution extends Component {
    render() {
        return (
            <div className="App">
                
                {/* <Hello name="Bruce" hero="Batman"/> */}
                {/* <RouterProject /> */}
                {/* <Message /> */}
                {/* <Welcome name="Bruce" hero="Batman"/> */}
                {/* <Counter /> */}
                {/* <FunctionClick />
                <ClassClick /> */}
                {/* <EventBind /> */}
                {/* <ParentComponent /> */}
                {/* <UserGreeting /> */}
                {/* <NameList /> */}
                {/* <Stylesheet primary={true}/> */}
                {/* <Inline /> */}
                {/* <Form /> */}
                {/* <LifecycleA /> */}
                {/* <FragmentDemo /> */}
                {/* <Table /> */}
                {/* <ParentComp /> */}
                {/* <RefsDemo /> */}
                {/* <FocusInput /> */}
                {/* <PortalDemo /> */}
                {/* <ErrorBoundary>
                    <Hero heroName="Batman" />
                    <Hero heroName="Superman" />
                    <Hero heroName="Joker" />
                </ErrorBoundary> */}
                {/* <ClickCounter />
                <HoverCounter /> */}
                {/* <ClickCountertwo />
                <HoverCounterTwo />
                <User render={(isLoggedIn) => isLoggedIn ?  'Alex' : 'Guest'} /> */}
                {/* <Counter 
                    render={(count, incrementCount) => (
                    <ClickCountertwo count={count} incrementCount={incrementCount}/>)} />
                <Counter 
                    render={(count, incrementCount) => (
                    <HoverCounterTwo count={count} incrementCount={incrementCount}/>)} />
                 */}

                {/* <UserProvider value="Alex"> */}
                    {/* <ComponentC /> */}
                {/* </UserProvider> */}
                 
            </div>
        )
    }
}
