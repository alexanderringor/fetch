import React, { Component } from 'react'

class UserGreeting extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             isLoggedIn: true
        }
    }
    

    render() {
        // 1ST APPROACH IF/ELSE APPROACH
        // if (this.state.isLoggedIn) {
        //     return <div>Welcome Alex</div>
        // } else {
        //     return <div>Welcome Guest</div>
        // }

        // 2ND APPROACH ELEMENT VARIABLES
        // let message
        // if (this.state.isLoggedIn) {
        //     message = <div>Welcome Alex</div>
        // } else {
        //     message = <div>Welcome Guest</div>
        // }

        // return(
        //     <div>{message}</div>
        // )
        
        // 3RD APPROACH TERNARY CONDITION OPERATOR

        // return (
        //     this.state.isLoggedIn ? <div>Welcome Alex</div> : <div>Welcome Guest</div>
        // )
        
        // 4TH APPROACH SHORT CIRCUIT OPERATOR
        return this.state.isLoggedIn && <div>Welcome Alex</div>

        
    }
}

export default UserGreeting
