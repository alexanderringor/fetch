import React, { Component } from 'react'

export class LifecycleB extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             name: 'Alex'
        }

        console.log('LifecycleB constructor')
    }

    static getDerivedStateFromProps(props,state) {
        console.log('LifecycleB getDerivedStateFromProps')
        return null
    }

    componentDidMount() {
        console.log('componentDidMount() B')
    }

    shouldComponentUpdate() {
        console.log('LifecycleB shouldComponentUpdate')
        return true
    }
    
    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('LifecycleB shouldComponentUpdate')
        return null
    }

    componentDidUpdate() {
        console.log('LifecycleB componentDidUpdate')
    }
    
    render() {
        console.log('Render B')         
            return <div>Livecycle B</div>
        
    }
}

export default LifecycleB
