import React, { Component } from 'react'
import ChildComponent from './ChildComponent'

class ParentComponent extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             parentName: 'Parent'
        }

        // this.greetParent = this.greetParent.bind(this)
    }

    greetParent = (child) => {
        alert(`Hello ${this.state.parentName} from ${child}`)
    }

    
    
    render() {
        return (
            // You can pass a method as props
            <div>
                
                <ChildComponent greetParent={this.greetParent.bind(this)} />
            </div>
        )
    }
}

export default ParentComponent
