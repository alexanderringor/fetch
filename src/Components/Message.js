import React, { Component } from 'react'

export default class Message extends Component {

    constructor() {
        super()
        this.state = {
            message: 'Welcome Visitor!',
            subscribed: true
        }
    }

    changeMessage() {
        
        if (!this.state.subscribed) {
            this.setState({message: 'Welcome Visitor!', subscribed: true})
            console.log(this.state.subscribed)
        } else {
            this.setState({message: 'Thanks for Subscribing', subscribed: false})
            
        }
    }

    render() {
        return (
            <div>
                <h1>Message Components</h1>
                <h4>{this.state.message}</h4>
                <button onClick={() => this.changeMessage()}>Subscribe</button>
            </div>
        )
    }
}
