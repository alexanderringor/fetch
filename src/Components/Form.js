import React, { Component } from 'react'

class Form extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             username: '',
             text: '',
             topic: 'react'
        }
    }
    
    handleUsernameChange = (event) => {
        this.setState({
            username: event.target.value
        }, () => {
            console.log(this.state.username) })
    }

    handleTextChange = e => {
        this.setState({
            text: e.target.value
        }, () => {
            console.log(this.state.text)
        })
    }
    
    handleTopicChange = e => {
        this.setState({
            topic: e.target.value
        })
    }

    handleSubmit = e => {
        e.preventDefault()
        alert(`${this.state.username} ${this.state.text} ${this.state.topic}`)
    }

    render() {
        return (
            <div>
                Form Component
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label>Username</label>
                        <input type="text" value={this.state.username} onChange={this.handleUsernameChange}></input>
                        <textarea value={this.state.text} onChange={this.handleTextChange}></textarea>
                    </div>

                    <div>
                        <label>Topic </label>
                        <select value={this.state.topic} onChange={this.handleTopicChange}>
                            <option value="react">React</option>
                            <option value="vue">Vue</option>
                            <option value="angular">Angular</option>
                        </select>
                    </div>
                    <button>Submit</button>
                </form>
            </div>
        )
    }
}

export default Form
