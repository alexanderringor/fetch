import React from 'react'

export default function NameList() {
    const names = ['Bruce', 'Clark', 'Diana']
    return (
        <div>
            {names.map((name, index) => {
                return (
                    <h6 key={index}>{name}</h6>
                )
            })}
        </div>
    )
}
 