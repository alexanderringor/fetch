import React, { Component } from 'react'
import RegularComponent from './RegularComponent'
import PureComponent from './PureComponent'

class ParentComp extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             name: 'Alex'
        }
    }
    
componentDidMount() {
    setInterval(() => {
        this.setState({
            name: 'Alex'
        })
    }, 2000)
}

    render() {
        console.log("ParentComp")
        return (
            <div>
                Parent Component
                {/* <RegularComponent name={this.state.name} /> */}
                {/* <PureComponent name={this.state.name} /> */}
            </div>
        )
    }
}

export default ParentComp
