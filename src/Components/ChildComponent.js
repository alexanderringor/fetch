import React from 'react'

function ChildComponent(props) {

    const {greetParent} = props
    return (
        <div>
            <button onClick={() => greetParent('child')}>Greet Parent</button>
        </div>
    )
}

export default ChildComponent
