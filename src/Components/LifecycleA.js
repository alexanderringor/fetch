import React, { Component } from 'react'
import LifecycleB from './LifecycleB'

export class LifecycleA extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             name: 'Alex'
        }

        console.log('LifecycleA constructor')
    }

    static getDerivedStateFromProps(props,state) {
        console.log('LifecycleA getDerivedStateFromProps')
        return null
    }

    componentDidMount() {
        console.log('componentDidMount() A')
    }

    shouldComponentUpdate() {
        console.log('LifecycleA shouldComponentUpdate')
        return true
    }
    
    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('LifecycleA shouldComponentUpdate')
        return null
    }

    componentDidUpdate() {
        console.log('LifecycleA componentDidUpdate')
    }
    
    changeState = () => {
        this.setState({
            name: 'anything but Alex'
        })
    }

    render() {
        console.log('Render A')         
            return <div>Livecycle A
                    <button onClick={this.changeState}>Change State</button>
                    <LifecycleB />
            </div>
        
    }
}

export default LifecycleA
