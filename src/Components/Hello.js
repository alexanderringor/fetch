import React from 'react'

const Hello = (props) => {
    // console.log(props)
    const {name, hero} = props
    return <h1>Hello {hero} {name}</h1>
}
 

export default Hello