import React, { Component } from 'react'

export default class Welcome extends Component {
    render() {
        const {name, hero: heroName} = this.props
        return (
            <div>
                <h1>Welcome {name} a.k.a {heroName}</h1>
            </div>
        )
    }
}
