import React, { Component } from 'react'

class EventBind extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             message: 'Hello'
        }
        
    }

    changeMessage = () => {
        this.setState({
            message: 'Goodbye'
        })
        // console.log(this)
    }

    // 4 ways to bind a handler under the render(2), in the class constructor(1) or (best practice) use arrow function in the class(1)
    // binding in render
    // arrow function in render
    // binding in class constructor
    // and  class property as arrow function
    
    render() {
        return (
            <div>
               {this.state.message}
               {/* first method */}
               {/* <button onClick={this.changeMessage}>Click</button>  */}
               {/* 2nd method */}
               <button onClick={() => {this.changeMessage()}}>Click</button> 
            </div>
        )
    }
}

export default EventBind
